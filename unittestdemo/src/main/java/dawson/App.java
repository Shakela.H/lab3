package dawson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static int echo(int x){
        return x;
    }

    public static int oneMore(int x){
        int y = x+1; // wasn't needed
        return x;
    }
}
