package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldReturnSameValue()
    {
        assertEquals("Echo should return the same value that was given", 5, App.echo(5));
    }

    @Test
    public void shouldReturnOneMoreThanGivenValue()
    {
        assertEquals("oneMore should return the value + 1", 6, App.oneMore(5));
    }
}
